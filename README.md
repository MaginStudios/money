<p align="center">
 <img src ="https://gitlab.com/MaginStudios/money/raw/master/money_app.png", height=350/>
</p>

# Money · Android App
This is a money tracking application. It includes the following operations:
- Add a new entry
- Edit an entry
- Delete an entry
- Get a list of entries

Technical details:
- **Kotlin** · Language
- **Clean Architecture** · Architecture
- **Androidx** · Android Extension Library
- **RxJava** · Reactive Programming
- **Dagger 2** · Dependency Injection
- **Room** · Persistence
- **Conductor** · Navigation
- **Butterknife** · View Injection
- **Timber** · Logging
- **JUnit, Mockito** · Unit Tests