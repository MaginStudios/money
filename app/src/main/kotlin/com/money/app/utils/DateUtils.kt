package com.money.app.utils

import java.text.SimpleDateFormat
import java.util.*

//TODO: internationalization
object DateUtils {
    fun formatDateTime(timestamp: Long) : String {
        val date = Calendar.getInstance()
        date.timeInMillis = timestamp
        var format = "HH:mm · dd/MM/yy"
        val tempDate = Calendar.getInstance()
        if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
            format = "HH:mm · 'Today'"
        } else {
            tempDate.add(Calendar.DATE, -1)
            if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
                format = "HH:mm · 'Yesterday'"
            }
        }
        return SimpleDateFormat(format, Locale.US).format(Date(date.timeInMillis))
    }

    fun formatDate(timestamp: Long) : String {
        val date = Calendar.getInstance()
        date.timeInMillis = timestamp
        val tempDate = Calendar.getInstance()
        if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
            return "Today"
        } else {
            tempDate.add(Calendar.DATE, -1)
            if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
                return "Yesterday"
            }
        }
        return SimpleDateFormat("dd/MM/yy", Locale.US).format(Date(date.timeInMillis))
    }

    fun formatTime(hour: Int, minute: Int) : String {
        return String.format("%02d",hour) + ":" + String.format("%02d",minute)
    }
}