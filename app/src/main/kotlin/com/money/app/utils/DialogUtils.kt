package com.money.app.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.text.format.DateFormat
import androidx.appcompat.app.AlertDialog
import com.money.app.R
import java.util.*

object DialogUtils {

    fun showConfirmationDialog(
        context: Activity,
        title: String,
        message: String,
        listener: DialogInterface.OnClickListener
    ): AlertDialog {
        val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.cancel)) {
                dialog, _ -> dialog.cancel()
        }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.accept), listener)
        alertDialog.show()
        return alertDialog
    }

    fun showTimePickerDialog(
        context: Context,
        hour: Int,
        minute: Int,
        listener: TimePickerDialog.OnTimeSetListener
    ): TimePickerDialog {
        val timePickerDialog = TimePickerDialog(
            context,
            listener,
            hour,
            minute,
            DateFormat.is24HourFormat(context)
        )
        timePickerDialog.show()
        return timePickerDialog
    }

    interface DatePickerListener {
        fun onDateSelected(dateSelected: Calendar)
    }

    fun showDatePickerDialog(
        activity: Activity,
        defaultDate: Calendar,
        listener: DatePickerListener
    ): DatePickerDialog {
        val pickerListener =
            DatePickerDialog.OnDateSetListener { _, yearFrom, monthOfYearFrom, dayOfMonthFrom ->
                val newDateFrom = Calendar.getInstance()
                newDateFrom.set(yearFrom, monthOfYearFrom, dayOfMonthFrom)
                listener.onDateSelected(newDateFrom)
            }
        val datePickerDialog = DatePickerDialog(
            activity, pickerListener, defaultDate.get(Calendar.YEAR), defaultDate.get(
                Calendar.MONTH
            ), defaultDate.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
        return datePickerDialog
    }
}