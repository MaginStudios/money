package com.money.app

import android.app.Application
import com.money.app.di.AppComponent
import com.money.app.di.DaggerAppComponent
import timber.log.Timber


class MoneyApp : Application() {
    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .application(this)
            .build()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}