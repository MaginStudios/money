package com.money.app.mvp

interface MvpPresenter {
    fun stop()
    fun destroy()
}