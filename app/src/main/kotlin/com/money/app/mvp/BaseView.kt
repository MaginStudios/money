package com.money.app.mvp

import android.content.Context
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.bluelinelabs.conductor.Controller


abstract class BaseView : Controller() {

    protected companion object {
        const val KEY_MONEY_ID = "moneyId"
    }

    val inject by lazy { injectDependencies() }

    override fun onContextAvailable(context: Context) {
        super.onContextAvailable(context)
        inject
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(getLayoutId(), container, false)
        ButterKnife.bind(this, view)
        setHasOptionsMenu(getMenuId() != 0)
        return view
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        setToolbarTitle()
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        getPresenter().stop()
    }

    override fun onDestroy() {
        getPresenter().destroy()
        super.onDestroy()
    }

    protected fun View.hideKeyboard() {
        val inputMethodManager = applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
    }

    protected fun showMessage(@StringRes msgResId: Int) {
        Toast.makeText(this.applicationContext, msgResId, Toast.LENGTH_SHORT).show()
    }

    private fun setToolbarTitle() {
        (activity as? AppCompatActivity)?.supportActionBar?.apply {
            title = resources?.getString(getToolbarTitleId())
            setDisplayHomeAsUpEnabled(router.backstackSize > 1)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (getMenuId() != 0) {
            inflater.inflate(getMenuId(), menu)
        }
    }

    @MenuRes
    open fun getMenuId(): Int {
        return 0
    }

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    @StringRes
    protected abstract fun getToolbarTitleId(): Int

    protected abstract fun injectDependencies()

    protected abstract fun getPresenter(): MvpPresenter
}