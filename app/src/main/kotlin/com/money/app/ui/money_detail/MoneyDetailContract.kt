package com.money.app.ui.money_detail

import android.widget.ImageView
import android.widget.TextView
import com.money.domain.model.Money

interface MoneyDetailContract {

    interface View {
        fun onDescriptionKeyboardAction(code: Int): Boolean
        fun onSaveMoneyAction()
        fun onAddMoneySuccess()
        fun onAddMoneyError(throwable: Throwable)
        fun onMoneyValidationFailed()
        fun onDecimalClicked()
        fun onNumberClicked(textView: TextView)
        fun onBackspaceClicked()
        fun onDescriptionFocusChanged(focused: Boolean)
        fun onEditMoneySuccess()
        fun onEditMoneyError(throwable: Throwable)
        fun onMoneyTypeClicked(button: ImageView)

        fun onLoadMoneySuccess(money: Money)
        fun onLoadMoneyError(throwable: Throwable)
        fun onDeleteMoneySuccess()
        fun onDeleteMoneyError(throwable: Throwable)
        fun onDeleteMoneyAction()
        fun showLoading()
        fun hideLoading()

        fun onTimeClicked()
        fun onDateClicked()
    }

    interface Presenter {
        fun addMoney(description: String, category: Int, amount: Double, timestamp: Long)
        fun loadMoney(id: Long)
        fun deleteMoney(id: Long)
        fun editMoney(id: Long, description: String, category: Int, amount: Double, timestamp: Long)
    }
}