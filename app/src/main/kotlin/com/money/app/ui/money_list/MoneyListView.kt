package com.money.app.ui.money_list

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.money.app.MoneyApp
import com.money.app.R
import com.money.app.ui.money_detail.MoneyDetailView
import com.money.app.mvp.BaseView
import com.money.app.mvp.MvpPresenter
import com.money.app.ui.common.initRecyclerView
import com.money.domain.model.Money
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.abs

class MoneyListView : BaseView(), MoneyListContract.View {

    @Inject
    lateinit var presenter: MoneyListPresenter

    @BindView(R.id.recycler_view)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.minus_symbol_text)
    lateinit var minusSymbolText: TextView

    @BindView(R.id.total_text)
    lateinit var totalText: TextView

    @BindView(R.id.empty_state_text)
    lateinit var emptyStateText: TextView

    private val clickListener: (Money) -> Unit = this::onMoneyClicked

    private val recyclerViewAdapter = MoneyListAdapter(clickListener)

    private val routerTransactionDuration : Long = 100

    override fun injectDependencies() {
        DaggerMoneyListComponent.builder()
            .appComponent(MoneyApp.component)
            .moneyListModule(MoneyListModule())
            .build()
            .inject(this)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        recyclerView.initRecyclerView(LinearLayoutManager(view.context), recyclerViewAdapter)
        with(presenter) {
            start(this@MoneyListView)
            loadMoneyList()
        }
    }

    private fun updateEmptyState() {
        emptyStateText.visibility = if (recyclerViewAdapter.itemCount == 0) View.VISIBLE else View.GONE
    }

    override fun onLoadMoneyListSuccess(moneyList: List<Money>) {
        recyclerViewAdapter.updateMoneyList(moneyList)
        updateEmptyState()
        val total = recyclerViewAdapter.getTotal()
        totalText.text = String.format("%.2f", abs(total))
        minusSymbolText.visibility = if (total < 0) View.VISIBLE else View.GONE
    }

    override fun onLoadMoneyListError(throwable: Throwable) {
        Timber.e(throwable)
        showMessage(R.string.money_load_error)
    }

    private fun onMoneyClicked(money: Money) {
        val moneyView = MoneyDetailView().apply {
            args.putLong(KEY_MONEY_ID, money.id)
        }
        router.pushController(
            RouterTransaction.with(moneyView)
                .popChangeHandler(FadeChangeHandler(routerTransactionDuration))
                .pushChangeHandler(FadeChangeHandler(routerTransactionDuration)))
    }

    @OnClick(R.id.fab)
    override fun onFabClicked() {
        router.pushController(RouterTransaction.with(MoneyDetailView())
            .popChangeHandler(FadeChangeHandler(routerTransactionDuration))
            .pushChangeHandler(FadeChangeHandler(routerTransactionDuration)))
    }

    override fun getLayoutId() = R.layout.money_list_view

    override fun getToolbarTitleId() = R.string.screen_title_money

    override fun getPresenter(): MvpPresenter = presenter
}