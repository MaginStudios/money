package com.money.app.ui.money_detail

import com.money.app.mvp.BasePresenter
import com.money.domain.interactor.AddMoneyUseCase
import com.money.domain.interactor.DeleteMoneyUseCase
import com.money.domain.interactor.EditMoneyUseCase
import com.money.domain.interactor.GetMoneyDetailUseCase
import com.money.domain.model.Money
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoneyDetailPresenter @Inject constructor(
    private val addMoneyUseCase: AddMoneyUseCase,
    private val getMoneyDetailUseCase: GetMoneyDetailUseCase,
    private val deleteMoneyUseCase: DeleteMoneyUseCase,
    private val editMoneyUseCase: EditMoneyUseCase
) : BasePresenter<MoneyDetailView>(), MoneyDetailContract.Presenter {

    override fun loadMoney(id: Long) {
        disposables.add(getMoneyDetailUseCase.get(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading() }
            .doFinally { view?.hideLoading() }
            .subscribe({ view?.onLoadMoneySuccess(it) }, { view?.onLoadMoneyError(it) }))
    }

    override fun deleteMoney(id: Long) {
        disposables.add(deleteMoneyUseCase.delete(Money(id))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ view?.onDeleteMoneySuccess() }, { view?.onDeleteMoneyError(it) }))
    }

    override fun editMoney(id: Long, description: String, category: Int, amount: Double, timestamp: Long) {
        disposables.add(editMoneyUseCase.edit(Money(id, description, category = category, amount = amount, timestamp = timestamp))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ view?.onEditMoneySuccess() }, this::onEditMoneyError))
    }

    override fun addMoney(description: String, category: Int, amount: Double, timestamp: Long) {
        disposables.add(addMoneyUseCase.add(Money(description = description, timestamp = timestamp, category = category, amount = amount))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ view?.onAddMoneySuccess() }, this::onAddMoneyError))
    }

    private fun onAddMoneyError(throwable: Throwable) {
        when(throwable) {
            is IllegalArgumentException -> view?.onMoneyValidationFailed()
            else -> view?.onAddMoneyError(throwable)
        }
    }

    private fun onEditMoneyError(throwable: Throwable) {
        when(throwable) {
            is IllegalArgumentException -> view?.onMoneyValidationFailed()
            else -> view?.onEditMoneyError(throwable)
        }
    }
}