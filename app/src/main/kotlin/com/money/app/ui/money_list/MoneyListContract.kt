package com.money.app.ui.money_list

import com.money.domain.model.Money

interface MoneyListContract {

    interface View {
        fun onLoadMoneyListSuccess(moneyList: List<Money>)
        fun onLoadMoneyListError(throwable: Throwable)
        fun onFabClicked()
    }

    interface Presenter {
        fun loadMoneyList()
    }
}