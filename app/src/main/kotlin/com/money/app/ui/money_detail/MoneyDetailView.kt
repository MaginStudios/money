package com.money.app.ui.money_detail

import android.app.TimePickerDialog
import android.content.DialogInterface
import android.graphics.PorterDuff
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.core.content.ContextCompat
import butterknife.*
import com.money.app.MoneyApp
import com.money.app.mvp.BaseView
import com.money.app.mvp.MvpPresenter
import com.money.app.utils.DialogUtils
import com.money.domain.model.Money
import timber.log.Timber
import java.text.DecimalFormatSymbols
import javax.inject.Inject
import kotlin.math.abs
import android.widget.TextView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.money.app.ui.common.Category
import com.money.app.R
import com.money.app.ui.common.CategoryAdapter
import com.money.app.utils.DateUtils
import java.util.*
import kotlin.collections.ArrayList

class MoneyDetailView : BaseView(), MoneyDetailContract.View {

    @Inject
    lateinit var detailPresenter: MoneyDetailPresenter

    @BindView(R.id.description_edit_text)
    lateinit var descriptionEditText: EditText

    @BindView(R.id.amount_text)
    lateinit var amountEditText: TextView

    @BindView(R.id.decimal_button_text)
    lateinit var decimalButtonText: TextView

    @BindView(R.id.time_text)
    lateinit var timeText: TextView

    @BindView(R.id.date_text)
    lateinit var dateText: TextView

    @BindView(R.id.keyboard_view)
    lateinit var keyboardView: ViewGroup

    @BindView(R.id.categories_spinner)
    lateinit var categoriesSpinner: Spinner

    @BindView(R.id.money_type_button_image)
    lateinit var moneyTypeButton: ImageView

    @BindView(R.id.progress_bar)
    lateinit var progressBar: ProgressBar

    var selectedDate: Calendar = Calendar.getInstance()

    override fun injectDependencies() {
        DaggerMoneyDetailComponent.builder()
            .appComponent(MoneyApp.component)
            .moneyDetailModule(MoneyDetailModule())
            .build()
            .inject(this)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        if (!isNewMoney()) {
            with(detailPresenter) {
                start(this@MoneyDetailView)
                loadMoney(getMoneyId())
            }
        } else {
            detailPresenter.start(this)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = super.onCreateView(inflater, container)
        decimalButtonText.text = DecimalFormatSymbols.getInstance().decimalSeparator.toString()
        val adapter = CategoryAdapter(
            activity!!,
            Category.values().toList() as ArrayList<Category>
        )
        adapter.setDropDownViewResource(R.layout.category_view)
        categoriesSpinner.adapter = adapter
        descriptionEditText.hint = categoriesSpinner.selectedItem.toString()
        fillDateAndTimeTexts()
        categoriesSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                descriptionEditText.hint = categoriesSpinner.selectedItem.toString()
            }
        }
        return view;
    }

    private fun fillDateAndTimeTexts() {
        timeText.text = DateUtils.formatTime(selectedDate.get(Calendar.HOUR_OF_DAY), selectedDate.get(Calendar.MINUTE))
        dateText.text = DateUtils.formatDate(selectedDate.timeInMillis)
    }

    @OnClick(R.id.time_container)
    override fun onTimeClicked() {
        DialogUtils.showTimePickerDialog(activity!!, Calendar.getInstance().get(Calendar.HOUR), Calendar.getInstance().get(Calendar.MINUTE),
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute -> run {
                selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay)
                selectedDate.set(Calendar.MINUTE, minute)
                timeText.text = DateUtils.formatTime(hourOfDay, minute)
            }})
    }

    @OnClick(R.id.date_container)
    override fun onDateClicked() {
        DialogUtils.showDatePickerDialog(activity!!, Calendar.getInstance(), object :
            DialogUtils.DatePickerListener {
            override fun onDateSelected(dateSelected: Calendar) {
                selectedDate.set(Calendar.YEAR, dateSelected.get(Calendar.YEAR))
                selectedDate.set(Calendar.MONTH, dateSelected.get(Calendar.MONTH))
                selectedDate.set(Calendar.DATE, dateSelected.get(Calendar.DATE))
                dateText.text = DateUtils.formatDate(selectedDate.timeInMillis)
            }
        })
    }

    override fun onDeleteMoneyAction() {
        DialogUtils.showConfirmationDialog(activity!!, activity!!.getString(R.string.deleteConfirmationTitle), activity!!.getString(R.string.deleteConfirmationMessage),
            DialogInterface.OnClickListener {
                    _, _ -> detailPresenter.deleteMoney(getMoneyId())
            })
    }

    override fun onLoadMoneySuccess(money: Money) {
        if (!isNewMoney()) {
            toggleMoneyType(money.amount < 0)
            for (i in 0 until categoriesSpinner.count) {
                val itemIdAtPosition = categoriesSpinner.getItemIdAtPosition(i).toInt()
                if (itemIdAtPosition == money.category) {
                    categoriesSpinner.setSelection(i)
                    break
                }
            }
        }
        descriptionEditText.setText(money.description)
        amountEditText.text = abs(money.amount).toString()
        val date = Calendar.getInstance()
        date.timeInMillis = money.timestamp
        selectedDate = date
        fillDateAndTimeTexts()
    }

    override fun onLoadMoneyError(throwable: Throwable) {
        Timber.e(throwable)
        showMessage(R.string.money_detail_load_error)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun onDeleteMoneySuccess() {
        showMessage(R.string.delete_money_success)
        router.popCurrentController()
    }

    override fun onDeleteMoneyError(throwable: Throwable) {
        Timber.e(throwable)
        showMessage(R.string.delete_money_failed)
    }

    @OnEditorAction(R.id.description_edit_text)
    override fun onDescriptionKeyboardAction(code: Int): Boolean {
        if (code == EditorInfo.IME_ACTION_DONE) {
            this.view?.hideKeyboard()
            descriptionEditText.clearFocus()
            return true
        }
        return false
    }

    @OnClick(R.id.money_type_button_image)
    override fun onMoneyTypeClicked(button: ImageView) {
        toggleMoneyType(button.tag == "income")
    }

    private fun toggleMoneyType(toMoney : Boolean) {
        if (toMoney) {
            val redColor = ContextCompat.getColor(moneyTypeButton.context, R.color.red)
            val lightRedColor = ContextCompat.getColor(moneyTypeButton.context, R.color.lightRed)
            moneyTypeButton.tag = "expense"
            moneyTypeButton.setImageResource(R.drawable.ic_minus_circle_outline)
            moneyTypeButton.drawable.setColorFilter(redColor, PorterDuff.Mode.SRC_ATOP)
            amountEditText.setTextColor(redColor)
            amountEditText.setHintTextColor(lightRedColor)
        } else {
            val greenColor = ContextCompat.getColor(moneyTypeButton.context, R.color.green)
            val lightGreenColor = ContextCompat.getColor(moneyTypeButton.context, R.color.lightGreen)
            moneyTypeButton.tag = "income"
            moneyTypeButton.setImageResource(R.drawable.ic_plus_circle_outline)
            moneyTypeButton.drawable.setColorFilter(greenColor, PorterDuff.Mode.SRC_ATOP)
            amountEditText.setTextColor(greenColor)
            amountEditText.setHintTextColor(lightGreenColor)
        }
    }

    @OnClick(R.id.decimal_button_text)
    override fun onDecimalClicked() {
        if (!amountEditText.text.contains(DecimalFormatSymbols.getInstance().decimalSeparator)) {
            amountEditText.text = amountEditText.text.toString().plus(DecimalFormatSymbols.getInstance().decimalSeparator)
        }
    }

    @OnClick(R.id.btnDel)
    override fun onBackspaceClicked() {
        if (amountEditText.text.isNotEmpty()) {
            amountEditText.text =
                amountEditText.text.subSequence(0, amountEditText.text.length - 1)
        }
    }

    @OnClick(R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9)
    override fun onNumberClicked(textView: TextView) {
        amountEditText.text = amountEditText.text.toString().plus(textView.text.toString())
    }

    @OnClick(R.id.btn_save)
    override fun onSaveMoneyAction() {
        val amountText = amountEditText.text.toString().replace(",",".")
        var amount = if (amountText.isEmpty() || amountText == ".") 0.0 else amountText.toDouble()
        if (moneyTypeButton.tag == "expense") {
            amount = -amount
        }
        val description = if (descriptionEditText.text!!.isEmpty()) categoriesSpinner.selectedItem.toString() else descriptionEditText.text.toString()
        if (isNewMoney()) {
            detailPresenter.addMoney(description, categoriesSpinner.selectedItemId.toInt(), amount, selectedDate.timeInMillis)
        } else {
            detailPresenter.editMoney(getMoneyId(), description, categoriesSpinner.selectedItemId.toInt(), amount, selectedDate.timeInMillis)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete -> onDeleteMoneyAction()
        }
        return true
    }

    private fun isNewMoney() : Boolean {
        return getMoneyId() == 0L
    }

    private fun getMoneyId() : Long {
        return args.getLong(KEY_MONEY_ID)
    }

    @OnFocusChange(R.id.description_edit_text)
    override fun onDescriptionFocusChanged(focused: Boolean) {
        if (!focused) {
            descriptionEditText.hideKeyboard()
        }
    }

    override fun onEditMoneySuccess() {
        showMessage(R.string.money_edit_success)
        router.popCurrentController()
    }

    override fun onEditMoneyError(throwable: Throwable) {
        Timber.e(throwable)
        showMessage(R.string.money_edit_failed)
    }

    override fun onAddMoneySuccess() {
        showMessage(R.string.money_add_success)
        router.popCurrentController()
    }

    override fun onMoneyValidationFailed() {
        showMessage(R.string.money_add_validation_failed)
    }

    override fun onAddMoneyError(throwable: Throwable) {
        Timber.e(throwable)
        showMessage(R.string.money_add_failed)
    }

    override fun getLayoutId() = R.layout.money_detail_view

    override fun getToolbarTitleId() = if (isNewMoney()) R.string.screen_title_add_money else R.string.screen_title_edit_money

    override fun getMenuId(): Int {
        return if (isNewMoney()) 0 else R.menu.edit_money_menu
    }

    override fun getPresenter(): MvpPresenter = detailPresenter
}