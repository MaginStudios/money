package com.money.app.ui.money_list

import com.money.app.mvp.BasePresenter
import com.money.domain.interactor.GetMoneyListUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoneyListPresenter @Inject constructor(private val moneyListUseCaseGet: GetMoneyListUseCase) : BasePresenter<MoneyListView>(), MoneyListContract.Presenter {

    override fun loadMoneyList() {
        disposables.add(moneyListUseCaseGet.load()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ view?.onLoadMoneyListSuccess(it) }, { view?.onLoadMoneyListError(it) }))
    }
}