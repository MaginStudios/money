package com.money.app.ui.money_list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.money.app.R
import com.money.app.ui.common.Category
import com.money.app.utils.DateUtils
import com.money.domain.model.Money
import kotlinx.android.synthetic.main.money_list_item.view.*
import kotlin.math.abs

class MoneyListAdapter(val onItemClick: (Money) -> Unit) : RecyclerView.Adapter<MoneyListAdapter.ViewHolder>() {

    val moneyList = mutableListOf<Money>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        moneyList[position].apply {
            holder.descriptionTextView.text = description
            holder.amountTextView.text = String.format("%.2f", abs(amount))
            val colorRes = if (amount < 0) R.color.red else R.color.green
            holder.amountTextView.setTextColor(ContextCompat.getColor(holder.amountTextView.context, colorRes))
            holder.currencyTextView.setTextColor(ContextCompat.getColor(holder.currencyTextView.context, colorRes))
            holder.dateTextView.text = DateUtils.formatDateTime(timestamp)
            holder.iconImageView.setImageResource(Category.findById(category).icon)
            holder.iconImageView.setColorFilter(ContextCompat.getColor(holder.iconImageView.context, Category.findById(category).color))
        }
    }

    override fun getItemCount() = moneyList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val moneyContainer = LayoutInflater.from(parent.context)
            .inflate(R.layout.money_list_item, parent, false) as ViewGroup
        val viewHolder = ViewHolder(moneyContainer)
        moneyContainer.setOnClickListener { onItemClick(moneyList[viewHolder.adapterPosition]) }
        return viewHolder
    }

    fun updateMoneyList(moneyList: List<Money>) {
        this.moneyList.clear()
        this.moneyList.addAll(moneyList)
        notifyDataSetChanged()
    }

    fun getTotal() : Double {
        return moneyList.fold(0.0, { total, money -> total + money.amount})
    }

    class ViewHolder(moneyContainer: ViewGroup) : RecyclerView.ViewHolder(moneyContainer) {
        val descriptionTextView: TextView = moneyContainer.money_description
        val currencyTextView: TextView = moneyContainer.money_currency
        val amountTextView: TextView = moneyContainer.money_amount
        val dateTextView: TextView = moneyContainer.date_container
        val iconImageView: ImageView = moneyContainer.money_icon
    }
}