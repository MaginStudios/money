package com.money.app.ui.money_list

import com.money.app.di.PerScreen
import com.money.domain.interactor.GetMoneyListUseCase
import com.money.domain.repository.MoneyRepository
import dagger.Module
import dagger.Provides

@Module
class MoneyListModule {

    @PerScreen
    @Provides
    fun provideMoneyListUseCase(moneyRepository: MoneyRepository) = GetMoneyListUseCase(moneyRepository)

    @PerScreen
    @Provides
    fun providePresenter(moneyListUseCaseGet: GetMoneyListUseCase) = MoneyListPresenter(moneyListUseCaseGet)
}