package com.money.app.ui.common

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.money.app.R

class CategoryAdapter(private val mContext: Context, list: ArrayList<Category>) :
    ArrayAdapter<Category>(mContext, 0, list) {
    private var list = ArrayList<Category>()

    init {
        this.list = list
    }

    override fun getItemId(position: Int): Long {
        return list[position].id.toLong()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listItem: View? = convertView
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.category_view, parent, false)

        val currentCategory = list[position]

        val icon = listItem!!.findViewById(R.id.icon) as ImageView
        icon.setImageResource(currentCategory.icon)
        icon.setColorFilter(ContextCompat.getColor(context, currentCategory.color))

        val name = listItem.findViewById(R.id.description) as TextView
        name.text = currentCategory.description

        return listItem
    }
}