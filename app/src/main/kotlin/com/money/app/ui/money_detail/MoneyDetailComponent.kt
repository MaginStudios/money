package com.money.app.ui.money_detail

import com.money.app.di.AppComponent
import com.money.app.di.PerScreen
import dagger.Component

@PerScreen
@Component(modules = arrayOf(MoneyDetailModule::class),
    dependencies = arrayOf(AppComponent::class))
interface MoneyDetailComponent {
    fun inject(detailView: MoneyDetailView)
}