package com.money.app.ui.money_list

import com.money.app.di.AppComponent
import com.money.app.di.PerScreen
import dagger.Component

@PerScreen
@Component(modules = arrayOf(MoneyListModule::class),
    dependencies = arrayOf(AppComponent::class))
interface MoneyListComponent {
    fun inject(view: MoneyListView)
}