package com.money.app.ui.common

import com.money.app.R

enum class Category(val id : Int, val description : String, val icon : Int, val color : Int) {
    FOOD(1, "Food", R.drawable.ic_food_fork_drink, R.color.catFood),
    SHOPPING(2, "Shopping", R.drawable.ic_shopping, R.color.catShopping),
    HOUSING(3, "Housing", R.drawable.ic_home, R.color.catHousing),
    TRANSPORTATION(4,"Transportation", R.drawable.ic_car, R.color.catTransportation),
    ENTERTAINMENT(5, "Entertainment", R.drawable.ic_human_handsup, R.color.catEntertainment),
    HEALTH(6, "Health", R.drawable.ic_heart_pulse, R.color.catHealth),
    BILLS(7, "Bills", R.drawable.ic_script_text, R.color.catBills),
    FINANCES(8, "Finances", R.drawable.ic_cash_multiple, R.color.catFinances),
    OTHERS(9, "Others", R.drawable.ic_receipt, R.color.catOthers);

    companion object {
        fun findById(id : Int) : Category {
            for (category in values()) {
                if (category.id == id) {
                    return category
                }
            }
            return FOOD
        }
    }

    override fun toString(): String {
        return description
    }
}