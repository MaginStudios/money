package com.money.app.ui.money_detail

import com.money.app.di.PerScreen
import com.money.domain.interactor.AddMoneyUseCase
import com.money.domain.interactor.DeleteMoneyUseCase
import com.money.domain.interactor.EditMoneyUseCase
import com.money.domain.interactor.GetMoneyDetailUseCase
import com.money.domain.repository.MoneyRepository
import dagger.Module
import dagger.Provides

@Module
class MoneyDetailModule {

    @PerScreen
    @Provides
    fun provideAddMoneyUseCase(moneyRepository: MoneyRepository) = AddMoneyUseCase(moneyRepository)

    @PerScreen
    @Provides
    fun provideGetMoneyDetailUseCase(moneyRepository: MoneyRepository) = GetMoneyDetailUseCase(moneyRepository)

    @PerScreen
    @Provides
    fun provideDeleteMoneyUseCase(moneyRepository: MoneyRepository) = DeleteMoneyUseCase(moneyRepository)

    @PerScreen
    @Provides
    fun provideEditMoneyUseCase(moneyRepository: MoneyRepository) = EditMoneyUseCase(moneyRepository)

    @PerScreen
    @Provides
    fun providePresenter(addMoneyUseCase: AddMoneyUseCase, getMoneyDetailUseCase: GetMoneyDetailUseCase, deleteMoneyUseCase: DeleteMoneyUseCase, editMoneyUseCase: EditMoneyUseCase) = MoneyDetailPresenter(addMoneyUseCase, getMoneyDetailUseCase, deleteMoneyUseCase, editMoneyUseCase)
}