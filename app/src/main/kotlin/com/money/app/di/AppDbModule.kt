package com.money.app.di

import android.content.Context
import com.money.data.repository.MoneyDaoImpl
import com.money.data.repository.MoneyModelMapperImpl
import com.money.data.repository.MoneyRepositoryImpl
import com.money.data.db.createMoneyDao
import com.money.domain.repository.MoneyRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppDbModule {

    @Singleton
    @Provides
    fun provideMoneyDao(context: Context) = createMoneyDao(context)

    @Singleton
    @Provides
    fun provideMoneyModelMapper() = MoneyModelMapperImpl()

    @Singleton
    @Provides
    fun provideMoneyRepository(moneyDao: MoneyDaoImpl, mapper: MoneyModelMapperImpl): MoneyRepository =
        MoneyRepositoryImpl(moneyDao, mapper)
}