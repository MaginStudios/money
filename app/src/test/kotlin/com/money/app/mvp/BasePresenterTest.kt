package com.money.app.mvp

import com.money.app.ui.money_list.MoneyListView
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

class BasePresenterTest {

    private lateinit var testSubject: BasePresenterUnderTest

    @Before
    fun setUp() {
        testSubject = spy(BasePresenterUnderTest())
    }

    @Test
    fun start_setsView() {
        val view = MoneyListView()

        testSubject.start(view)

        verify(testSubject).start(view)
    }
}

class BasePresenterUnderTest : BasePresenter<MoneyListView>()