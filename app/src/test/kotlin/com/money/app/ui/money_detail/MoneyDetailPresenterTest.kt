package com.money.app.ui.money_detail


import com.money.domain.interactor.AddMoneyUseCase
import com.money.domain.interactor.DeleteMoneyUseCase
import com.money.domain.interactor.EditMoneyUseCase
import com.money.domain.interactor.GetMoneyDetailUseCase
import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.inOrder

class MoneyDetailPresenterTest {

    private lateinit var testSubject: MoneyDetailPresenter

    private val view: MoneyDetailView = mock()

    private val repository: MoneyRepository = mock()

    private val getMoneyDetailUseCase: GetMoneyDetailUseCase = GetMoneyDetailUseCase(repository)

    private val deleteMoneyUseCase: DeleteMoneyUseCase = DeleteMoneyUseCase(repository)

    private val addMoneyUseCase: AddMoneyUseCase = AddMoneyUseCase(repository)

    private val editMoneyUseCase: EditMoneyUseCase = EditMoneyUseCase(repository)

    private val testScheduler = TestScheduler()

    private val money = Money(1L, "some description", 2L, 1, 11.0)

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler({ _ -> testScheduler })
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }

        testSubject = MoneyDetailPresenter(addMoneyUseCase, getMoneyDetailUseCase, deleteMoneyUseCase, editMoneyUseCase)
        testSubject.start(view)
    }

    @Test
    fun loadMoney_givenFindMoneyByIdSuccess_callsViewOnLoadMoneySuccess() {
        whenever(repository.findMoneyById(money.id)).thenReturn(Maybe.just(money))

        testSubject.loadMoney(money.id)
        testScheduler.triggerActions()

        verify(view).onLoadMoneySuccess(money)
    }

    @Test
    fun loadMoney_givenFindMoneyByIdSuccess_andViewNotAttached_doesNotCallViewOnLoadMoneySuccess() {
        whenever(repository.findMoneyById(money.id)).thenReturn(Maybe.just(money))
        testSubject.stop()

        testSubject.loadMoney(money.id)
        testScheduler.triggerActions()

        verify(view, never()).onLoadMoneySuccess(money)
    }

    @Test
    fun loadMoney_givenFindMoneyByIdError_callsViewOnLoadMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.findMoneyById(money.id)).thenReturn(Maybe.error(throwable))

        testSubject.loadMoney(money.id)
        testScheduler.triggerActions()

        verify(view).onLoadMoneyError(throwable)
    }

    @Test
    fun loadMoney_givenFindMoneyByIdError_andViewNotAttached_doesNotCallViewOnLoadMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.findMoneyById(money.id)).thenReturn(Maybe.error(throwable))
        testSubject.stop()

        testSubject.loadMoney(money.id)
        testScheduler.triggerActions()

        verify(view, never()).onLoadMoneyError(throwable)
    }

    @Test
    fun loadMoney_callsViewShowLoading_andThenHideLoading() {
        whenever(repository.findMoneyById(money.id)).thenReturn(Maybe.just(money))

        testSubject.loadMoney(money.id)
        testScheduler.triggerActions()

        val inOrder = inOrder(view, view)
        inOrder.verify(view).showLoading()
        inOrder.verify(view).hideLoading()
    }

    @Test
    fun deleteMoney_givenDeleteMoneySuccess_callsViewOnDeleteMoneySuccess() {
        whenever(repository.delete(any())).thenReturn(Completable.complete())

        testSubject.deleteMoney(money.id)
        testScheduler.triggerActions()

        verify(view).onDeleteMoneySuccess()
    }

    @Test
    fun deleteMoney_givenDeleteMoneySuccess_andViewNotAttached_doesNotCallViewOnDeleteMoneySuccess() {
        whenever(repository.delete(any())).thenReturn(Completable.complete())
        testSubject.stop()

        testSubject.deleteMoney(money.id)
        testScheduler.triggerActions()

        verify(view, never()).onDeleteMoneySuccess()
    }

    @Test
    fun deleteMoney_givenDeleteMoneyError_callsViewOnDeleteMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.delete(any())).thenReturn(Completable.error(throwable))

        testSubject.deleteMoney(money.id)
        testScheduler.triggerActions()

        verify(view).onDeleteMoneyError(throwable)
    }

    @Test
    fun deleteMoney_givenDeleteMoneyError_andViewNotAttached_doesNotCallViewOnDeleteMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.delete(any())).thenReturn(Completable.error(throwable))
        testSubject.stop()

        testSubject.deleteMoney(money.id)
        testScheduler.triggerActions()

        verify(view, never()).onDeleteMoneyError(throwable)
    }

    @Test
    fun editMoney_givenEditMoneySuccess_callsViewOnEditMoneySuccess() {
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.complete())

        testSubject.editMoney(money.id, money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view).onEditMoneySuccess()
    }

    @Test
    fun editMoney_givenEditMoneySuccess_andViewNotAttached_doesNotCallViewOnEditMoneySuccess() {
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.complete())
        testSubject.stop()

        testSubject.editMoney(money.id, money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view, never()).onEditMoneySuccess()
    }

    @Test
    fun editMoney_givenEditMoneyError_callsViewOnEditMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.error(throwable))

        testSubject.editMoney(money.id, money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view).onEditMoneyError(throwable)
    }

    @Test
    fun editMoney_givenEditMoneyError_andViewNotAttached_doesNotCallViewOnEditMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.error(throwable))
        testSubject.stop()

        testSubject.editMoney(money.id, money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view, never()).onEditMoneyError(throwable)
    }

    @Test
    fun addMoney_givenAddMoneySuccess_callsViewOnAddMoneySuccess() {
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.complete())

        testSubject.addMoney(money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view).onAddMoneySuccess()
    }

    @Test
    fun addMoney_givenAddMoneySuccess_andViewNotAttached_doesNotCallViewOnAddMoneySuccess() {
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.complete())
        testSubject.stop()

        testSubject.addMoney(money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view, never()).onAddMoneySuccess()
    }

    @Test
    fun editMoney_givenAddMoneyError_callsViewOnAddMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.error(throwable))

        testSubject.addMoney(money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view).onAddMoneyError(throwable)
    }

    @Test
    fun addMoney_givenAddMoneyError_andViewNotAttached_doesNotCallViewOnAddMoneyError() {
        val throwable = RuntimeException()
        whenever(repository.insertOrUpdate(any())).thenReturn(Completable.error(throwable))
        testSubject.stop()

        testSubject.addMoney(money.description, money.category, money.amount, money.timestamp)
        testScheduler.triggerActions()

        verify(view, never()).onAddMoneyError(throwable)
    }
}