package com.money.app.ui.money_list


import com.money.domain.interactor.GetMoneyListUseCase
import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class MoneyListPresenterTest {

    private lateinit var testSubject: MoneyListPresenter

    private val view: MoneyListView = mock()

    private val repository: MoneyRepository = mock()

    private val moneyListUseCaseGet: GetMoneyListUseCase = GetMoneyListUseCase(repository)

    private val testScheduler = TestScheduler()

    private val moneyList = arrayListOf(Money(1L, "some text"))

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler({ _ -> testScheduler })
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }

        testSubject = MoneyListPresenter(moneyListUseCaseGet)
        testSubject.start(view)
    }

    @Test
    fun loadMoneyList_givenLoadMoneyListSuccess_callsViewOnLoadMoneyListSuccess() {
        whenever(repository.getAllMoneyList()).thenReturn(Single.just(moneyList))

        testSubject.loadMoneyList()
        testScheduler.triggerActions()

        verify(view).onLoadMoneyListSuccess(moneyList)
    }

    @Test
    fun loadMoneyList_givenLoadMoneyListSuccess_andViewNotAttached_doesNotCallViewOnLoadMoneyListSuccess() {
        whenever(repository.getAllMoneyList()).thenReturn(Single.just(moneyList))
        testSubject.stop()

        testSubject.loadMoneyList()
        testScheduler.triggerActions()

        verify(view, never()).onLoadMoneyListSuccess(moneyList)
    }

    @Test
    fun loadMoneyList_givenLoadMoneyListError_callsViewOnLoadMoneyListError() {
        val throwable = RuntimeException()
        whenever(repository.getAllMoneyList()).thenReturn(Single.error(throwable))

        testSubject.loadMoneyList()
        testScheduler.triggerActions()

        verify(view).onLoadMoneyListError(throwable)
    }

    @Test
    fun loadMoneyList_givenLoadMoneyListError_andViewNotAttached_doesNotCallViewOnLoadMoneyListError() {
        val throwable = RuntimeException()
        whenever(repository.getAllMoneyList()).thenReturn(Single.error(throwable))
        testSubject.stop()

        testSubject.loadMoneyList()
        testScheduler.triggerActions()

        verify(view, never()).onLoadMoneyListError(throwable)
    }
}