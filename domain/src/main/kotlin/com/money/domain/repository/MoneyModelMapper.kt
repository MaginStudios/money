package com.money.domain.repository

interface MoneyModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}