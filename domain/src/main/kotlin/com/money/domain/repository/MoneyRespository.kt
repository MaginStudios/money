package com.money.domain.repository

import com.money.domain.model.Money
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface MoneyRepository {

    fun insertOrUpdate(money: Money): Completable

    fun delete(money: Money): Completable

    fun findMoneyById(id: Long): Maybe<Money>

    fun getAllMoneyList(): Single<List<Money>>
}