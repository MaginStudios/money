package com.money.domain.interactor

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Completable

class EditMoneyUseCase(private val repository: MoneyRepository) {
    fun edit(money: Money): Completable = validate(money).andThen(repository.insertOrUpdate(money))

    private fun validate(money: Money): Completable {
        return if (!money.isValidForEdit()) {
            Completable.error(IllegalArgumentException("Money failed validation before edit"))
        } else {
            Completable.complete()
        }
    }
}