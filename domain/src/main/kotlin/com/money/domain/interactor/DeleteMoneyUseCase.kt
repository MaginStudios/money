package com.money.domain.interactor

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Completable

class DeleteMoneyUseCase(private val repository: MoneyRepository) {
    fun delete(money: Money): Completable = repository.delete(money)
}