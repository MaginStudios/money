package com.money.domain.interactor

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Single

class GetMoneyListUseCase(private val repository: MoneyRepository) {
    fun load(): Single<List<Money>> = repository.getAllMoneyList()
}