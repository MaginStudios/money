package com.money.domain.interactor

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Maybe

class GetMoneyDetailUseCase(private val repository: MoneyRepository) {
    fun get(id: Long): Maybe<Money> = repository.findMoneyById(id)
}