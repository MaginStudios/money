package com.money.domain.interactor

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Completable

class AddMoneyUseCase(private val repository: MoneyRepository) {
    fun add(money: Money): Completable = validate(money).andThen(repository.insertOrUpdate(money))

    private fun validate(money: Money): Completable {
        return if (!money.isValidForAdd()) {
            Completable.error(IllegalArgumentException("Money failed validation before add"))
        } else {
            Completable.complete()
        }
    }
}