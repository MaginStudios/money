package com.money.domain.model

data class Money(
    val id: Long = 0,
    val description: String = "",
    val timestamp: Long = 0,
    val category: Int = 0,
    val amount: Double = 0.0
) {

    fun isValidForEdit() = id > 0 && description.trim().isNotEmpty()

    fun isValidForAdd() = description.trim().isNotEmpty()
}