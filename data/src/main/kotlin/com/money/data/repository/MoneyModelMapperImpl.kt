package com.money.data.repository

import com.money.data.entity.MoneyEntity
import com.money.domain.model.Money
import com.money.domain.repository.MoneyModelMapper

class MoneyModelMapperImpl : MoneyModelMapper<MoneyEntity, Money> {
    override fun fromEntity(from: MoneyEntity) = Money(from.id, from.description, from.timestamp, from.category, from.amount)
    override fun toEntity(from: Money) = MoneyEntity(
        from.id,
        from.description,
        from.timestamp,
        from.category,
        from.amount
    )
}