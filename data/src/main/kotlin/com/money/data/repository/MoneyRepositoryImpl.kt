package com.money.data.repository

import com.money.domain.model.Money
import com.money.domain.repository.MoneyRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class MoneyRepositoryImpl(private val moneyDao: MoneyDaoImpl,
                          private val mapper: MoneyModelMapperImpl
) : MoneyRepository {

    override fun insertOrUpdate(money: Money): Completable = Completable.fromAction { moneyDao.insertOrUpdate(mapper.toEntity(money)) }

    override fun delete(money: Money): Completable = Completable.fromAction { moneyDao.delete(mapper.toEntity(money)) }

    override fun findMoneyById(id: Long): Maybe<Money> {
        return moneyDao.findMoneyById(id)
            .map { mapper.fromEntity(it) }
    }

    override fun getAllMoneyList(): Single<List<Money>> {
        return moneyDao.getAllMoney()
            .map { it.map(mapper::fromEntity) }
    }
}