package com.money.data.repository

import androidx.room.*
import com.money.data.entity.MoneyEntity
import com.money.domain.repository.MoneyDao
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface MoneyDaoImpl : MoneyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(money: MoneyEntity)

    @Delete
    fun delete(money: MoneyEntity)

    @Query("SELECT * FROM money WHERE id = :id")
    fun findMoneyById(id: Long): Maybe<MoneyEntity>

    @Query("SELECT * FROM money ORDER BY timestamp DESC")
    fun getAllMoney(): Single<List<MoneyEntity>>
}