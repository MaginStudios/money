package com.money.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.money.data.entity.MoneyEntity
import com.money.data.repository.MoneyDaoImpl

fun createMoneyDao(context: Context): MoneyDaoImpl {
    return Room.databaseBuilder(context, MoneyDatabase::class.java, "moneydb")
        .build().moneyDao()
}

@Database(entities = arrayOf(MoneyEntity::class), version = 1, exportSchema = false)
internal abstract class MoneyDatabase : RoomDatabase() {
    abstract fun moneyDao(): MoneyDaoImpl
}