package com.money.data

import com.money.data.entity.MoneyEntity
import com.money.data.repository.MoneyDaoImpl
import com.money.data.repository.MoneyModelMapperImpl
import com.money.data.repository.MoneyRepositoryImpl
import com.money.domain.model.Money
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Maybe
import io.reactivex.Single
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.hamcrest.Matchers.`is` as is_

class MoneyRepositoryImplTest {

    private val moneyDao: MoneyDaoImpl = mock()

    private val moneyMapper: MoneyModelMapperImpl = mock()

    private val testSubject: MoneyRepositoryImpl =
        MoneyRepositoryImpl(moneyDao, moneyMapper)

    @Test
    fun insertOrUpdate_emitsOnSuccess() {
        val money = money()
        val moneyEntity = given_entityFromMoney(money)

        val testObserver = testSubject.insertOrUpdate(money).test()

        testObserver.assertComplete()
        argumentCaptor<MoneyEntity>().apply {
            verify(moneyDao).insertOrUpdate(capture())
            assertThat(firstValue.id, is_(moneyEntity.id))
            assertThat(firstValue.description, is_(moneyEntity.description))
            assertThat(firstValue.timestamp, is_(moneyEntity.timestamp))
            assertThat(firstValue.category, is_(moneyEntity.category))
            assertThat(firstValue.amount, is_(moneyEntity.amount))
        }
    }

    @Test
    fun delete_emitsOnSuccess() {
        val money = money()
        val moneyEntity = given_entityFromMoney(money)

        val testObserver = testSubject.delete(money).test()

        testObserver.assertComplete()
        argumentCaptor<MoneyEntity>().apply {
            verify(moneyDao).delete(capture())
            assertThat(firstValue.id, is_(moneyEntity.id))
            assertThat(firstValue.description, is_(moneyEntity.description))
            assertThat(firstValue.timestamp, is_(moneyEntity.timestamp))
            assertThat(firstValue.category, is_(moneyEntity.category))
            assertThat(firstValue.amount, is_(moneyEntity.amount))
        }
    }

    @Test
    fun findMoneyById_givenItemFound_emitsMoney() {
        val moneyEntity = moneyEntity()
        val money = given_moneyFromEntity(moneyEntity)
        whenever(moneyDao.findMoneyById(any())).thenReturn(Maybe.just(moneyEntity))

        val testObserver = testSubject.findMoneyById(money.id).test()

        testObserver.assertResult(money)
        argumentCaptor<Long>().apply {
            verify(moneyDao).findMoneyById(capture())
            assertThat(firstValue, is_(money.id))
        }
        verify(moneyMapper).fromEntity(moneyEntity)
    }

    @Test
    fun findMoneyById_givenItemNotFound_emitsEmpty() {
        whenever(moneyDao.findMoneyById(any())).thenReturn(Maybe.empty())

        val testObserver = testSubject.findMoneyById(1).test()

        testObserver.assertComplete()
        testObserver.assertNoValues()
    }

    @Test
    fun getAllMoney_emitsValues() {
        val entity = moneyEntity()
        val moneyEntities = listOf(entity)
        val money = given_moneyFromEntity(entity)
        whenever(moneyDao.getAllMoney()).thenReturn(Single.just(moneyEntities))

        val testObserver = testSubject.getAllMoneyList().test()

        verify(moneyMapper).fromEntity(entity)
        testObserver.assertResult(listOf(money))
        testObserver.assertComplete()
    }

    private fun money() = Money(1, "test money", 9L, 2, 11.5)

    private fun moneyEntity() =
        MoneyEntity(2, "test entity", 10L, 1, 10.5)

    private fun given_entityFromMoney(money: Money): MoneyEntity {
        val moneyEntity: MoneyEntity = mock()
        whenever(moneyMapper.toEntity(money)).thenReturn(moneyEntity)
        whenever(moneyEntity.id).thenReturn(money.id)
        whenever(moneyEntity.description).thenReturn(money.description)
        whenever(moneyEntity.timestamp).thenReturn(money.timestamp)
        whenever(moneyEntity.category).thenReturn(money.category)
        whenever(moneyEntity.amount).thenReturn(money.amount)
        return moneyEntity
    }

    private fun given_moneyFromEntity(moneyEntity: MoneyEntity): Money {
        val money: Money = mock()
        whenever(moneyMapper.fromEntity(moneyEntity)).thenReturn(money)
        whenever(money.id).thenReturn(moneyEntity.id)
        whenever(money.description).thenReturn(moneyEntity.description)
        whenever(money.timestamp).thenReturn(moneyEntity.timestamp)
        whenever(money.category).thenReturn(moneyEntity.category)
        whenever(money.amount).thenReturn(moneyEntity.amount)
        return money
    }
}