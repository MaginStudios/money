package com.money.data

import com.money.data.entity.MoneyEntity
import com.money.data.repository.MoneyModelMapperImpl
import com.money.domain.model.Money
import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertTrue

class MoneyModelMapperImplTest {

    private lateinit var testSubject: MoneyModelMapperImpl

    companion object {
        private const val ID = 1L
        private const val MONEY_DESCRIPTION = "money description"
        private const val MONEY_TIMESTAMP = 2L
        private const val MONEY_CATEGORY = 1
        private const val MONEY_AMOUNT = 10.5
    }

    @Before
    fun setUp() {
        testSubject = MoneyModelMapperImpl()
    }

    @Test
    fun fromEntity_convertsFromEntity() {
        val entity = MoneyEntity(
            ID,
            MONEY_DESCRIPTION,
            MONEY_TIMESTAMP,
            MONEY_CATEGORY,
            MONEY_AMOUNT
        )

        val result = testSubject.fromEntity(entity)

        assertTrue(result.id == ID)
        assertTrue(result.description == MONEY_DESCRIPTION)
        assertTrue(result.timestamp == MONEY_TIMESTAMP)
        assertTrue(result.category == MONEY_CATEGORY)
        assertTrue(result.amount == MONEY_AMOUNT)
    }

    @Test
    fun toEntity_convertsToEntity() {
        val money = Money(ID, MONEY_DESCRIPTION, MONEY_TIMESTAMP, MONEY_CATEGORY, MONEY_AMOUNT)

        val result = testSubject.toEntity(money)

        assertTrue(result.id == ID)
        assertTrue(result.description == MONEY_DESCRIPTION)
        assertTrue(result.description == MONEY_DESCRIPTION)
        assertTrue(result.timestamp == MONEY_TIMESTAMP)
        assertTrue(result.category == MONEY_CATEGORY)
        assertTrue(result.amount == MONEY_AMOUNT)
    }
}